commands:

build with docker:
docker build -t mpet .
docker build -t ubupy35 .
docker build -t ubupy34 .

docker garbage collect:
#!/bin/bash
# remove exited containers:
docker ps --filter status=dead --filter status=exited -aq | xargs -r docker rm -v
# remove unused images:
docker images --no-trunc | grep '<none>' | awk '{ print $3 }' | xargs -r docker rmi




run bash:
docker run -it mpetalpha bash
docker run -it -v /c/Users/deniz/stuff:/app/wkdir mpetalpha bash

determine 64/32 bit in bash:
uname -m

determine 64/32 bit in python:
python -c 'import struct;print( 8 * struct.calcsize("P"))'


run MPET
python ../mpet/mpetrun.py ./params_system.cfg
xvfb-run python ../mpet/mpetplot.py sim_output text

run MPET from docker external
docker run -it -v /c/Users/deniz/stuff:/app/wkdir mpet mpetrun.sh params_system.cfg
docker run -it -v /c/Users/deniz/stuff:/app/wkdir mpet mpetplot.sh sim_output curr save




install strings
apt-get update
apt-get install binutils



apt-get install libgfortran3



https://sourceforge.net/projects/pyqt/files/PyQt5/PyQt-5.8.2/PyQt5_gpl-5.8.2.tar.gz


apt-get install wget

wget https://sourceforge.net/projects/pyqt/files/PyQt5/PyQt-5.8.2/PyQt5_gpl-5.8.2.tar.gz
tar -xvf PyQt5_gpl-5.8.2.tar.gz


export PYTHONPATH=/usr/lib/python3/dist-packages




# anaconda installation:

conda create -y --name py34 python=3.4
source activate py34

conda install -n py34 -y --file requirements.txt


# libGL
apt-get update
apt-get install mayavi2 


export PATH=/opt/conda/envs/py34/bin:$PATH$







apt-get install python3
apt-get install python3-numpy python3-scipy python3-matplotlib python3-pyqt5 mayavi2 python3-lxml


docker run -it -v /c/Users/deniz/stuff:/app/wkdir mpetalpha bash