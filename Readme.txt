This folder contains the source code to create a docker container that allows to run  the simulation environement MPET by the Bazant  group. See https://bitbucket.org/bazantgroup/mpet

Docker integration by Deniz Bozyigit, Battrion AG, 2017-06-19

Install:
1) Install docker. Verify docker commands work.

2) Go to folder ubupy35. Run: 
		docker build -t ubupy35 .

	This should take about 10-15min and requires about 1.5GB of space!
	
3) Go to folder mpet. Run:
		docker build -t mpet .

	This should take about 3min and requires about 0.5GB of space!

4) Choose a working directory (here: /c/Users/deniz/wd)
5) Run MPET with the following commands:
	docker run -it -v /c/Users/deniz/wd:/app/wkdir mpet mpetrun.sh params_system.cfg
	docker run -it -v /c/Users/deniz/wd:/app/wkdir mpet mpetplot.sh sim_output curr save

6) Alternatively, open a bash with:
	docker run -it -v /c/Users/deniz/wd:/app/wkdir mpet bash

	

		
		